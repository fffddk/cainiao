#include <stdio.h>
#include<math.h>
#include "calculate.h"
#include <stdbool.h>
// 验证隔得巴德猜想
// 任意一个大于2的偶数都能拆成两个素数的和

bool Isprime(int a)//bool?
{
    if (a == 2)
        return true;
    for (int i = 2; i <= sqrt(a); i++)
    {
        if (a % i == 0)
            return false;
    }
    return true;
}

int main()
{
    int a;

    scanf("%d", &a);
    if (a % 2 != 0)
    {
        printf("请输入一个偶数\n");
        return 0;
    }
    //
    for(int i=2;i<=a/2;i++)
    {   //i和a-i都是素数则成立
        if(Isprime(i)==true && Isprime(a-1)==true)
        {
            printf("first :%d  second:%d\n",i,a-1);
        }
    }

    return 0;
}