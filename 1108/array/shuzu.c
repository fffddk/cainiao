#include <stdio.h>
#define Arraylen(a) sizeof(a)/sizeof(a[0])
void Printarray(int *a,int len)
{
    printf("size:%d\n", sizeof(a));
    for (int i = 0; i < len; i++)
    {
        printf("%d ", a[i]);
    }
    printf("\n");
}


int main()
{
    // 定义一个数组，每个元素都是int型，容量是3
    int a[] = {1, 2, 3,232,453543};
    // 取数组元素
    // 数组的开始位置是0
    // 数组访问不允许越界
    // 数组遍历
    //  for(int i=0;i<3;i++)
    //  {
    //      printf("%d ",a[i]);
    //  }
    //  printf("\n");

    // 数组的大小，内存的排布
    //  printf("size:%d\n",sizeof(a));
    //  for(int i=0;i<3;i++)
    //  {
    //      printf("%d ",&a[i]);
    //  }
    //  printf("\n");
   // Printarray(a,Arraylen(a));


    //a就是数组的元素首地址
   printf("%x %x %x\n",a,&a[0],&a);
   printf("%x %x\n",a+1,&a[1],&a+1);
   
    return 0;
}