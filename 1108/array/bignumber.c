#include <stdio.h>

int main()
{
    // 2^100=? 一个int塞一位
    int a[100] = {0};
    a[0] = 1;
    // 乘100次2
    for (int i = 0; i < 100; i++) // 位数
    {
        for (int j = 0; j < 100; j++)
        {
            a[j] = a[j] * 2;
        }
        for (int j = 0; j < 99; j++) // 取不到99，要是取到，j+1=100越界
        {
            if (a[j] >= 10)
            {
                a[j + 1]++;
                a[j] -= 10;
            }
        }
    }
    // 去掉前面的零
    // 从第一个不为零的数开始打印
    int flag = 0;
    for (int i = 99; i >= 0; i--)
    {
        if (a[i] != 0)
            flag = 1;
        if (flag == 1)
            printf("%d", a[i]);
    }
    printf("\n");
    return 0;
}