#include <stdio.h>

int main()
{
    // 数据类型
    // 指针也是变量，存的是地址

    int *a;
    char *b;
    double *c;
    // 指针的大小：8个字节，只和操作系统的位数有关  2^32=4个G
    printf("%d\n", sizeof(a));
    printf("%d\n", sizeof(b));
    printf("%d\n", sizeof(c));

    // 指针的赋值  &
    int v = 10;
    a = &v;
    printf("a: %d\n", a);
    // 把变量v的地址给到指针a
    // a指向变量v

    // 取指针指向地址的内存值  *
    printf("*a:%d\n", *a);

    // 将指针指向的地址的内存的值为20
    *a = 20;
    printf("v: %d\n", v);



    // 野指针和空指针
    // 指针指向非法为止：野指针
    // 空指针：指向地址0的指针
    //  int * p=2;
    //  int * p=NULL;
    //  printf("%d\n",*p);
    // 定义一个指针时，要初始化为空指针
    int *p = NULL;


    
    return 0;
}
