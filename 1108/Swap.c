#include<stdio.h>
void Swap(int *a,int *b)
{
    int temp=*a;
    *a=*b;
    *b=temp;//交换a，b的内存
   
}
int main()
{
    int a=10;
    int b=20;

    Swap(&a,&b);
    printf(" main:a:%d  b:%d\n",a,b);
    return 0;
}