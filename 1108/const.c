#include <stdio.h>
int main()
{
    //修饰a成为一个常量，
    const int a = 10;


    //const修饰指针，指针指向的内存不可修改：常量指针  指针所指的值无法改
    // const int *p =&a;
    // // *p=20;
    // int b=20;
    // p=&b;
    // printf("%d\n", a);


   // 变量p本身是个常量，它的值不可修改：指针常量  常量的指向不能改
    int *const p=&a;
    
    int b =20;
   // p=&b;
   *p=b;
    printf("%d\n", a);
    return 0;
}