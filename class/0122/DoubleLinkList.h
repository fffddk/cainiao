#ifndef _DOUBLELINKLIST_H_
#define _DOUBLELINKLIST_H_
#define ElementType int

class DLlist
{
public:
    DLlist();
    ~DLlist();
    void InsertDLlistTail(DLlist& list,ElementType element);
    void InsertDLlistHead(DLlist& list,ElementType element);
    void RemoveByIndex(DLlist& list,int index,void(*freeptr)(ElementType));
    void RemoveByElement(DLlist& list,ElementType element,bool(*Operptr)(ElementType,ElementType),void(*freeptr)(ElementType));
    ElementType* FindByIndex(DLlist& list,int index);
    DLlist FindByElement(DLlist& list,ElementType element,bool(*OperPtr)(ElementType,ElementType));
    void TravelDLlist(DLlist& list,void(*funcptr)(ElementType));
    void DLLlistSort(DLlist* list,bool(*funptr)(ElementType,ElementType));
private:
    struct DLNode
    {
        ElementType data;
        struct DLNode* prev;
        struct DLNode* next;
    };
    void FastSort(DLNode* start,DLNode* end,bool(*funcptr)(ElementType,ElementType));
    int len;
    struct DLNode* head;
    struct DLNode* tail;
};

#endif