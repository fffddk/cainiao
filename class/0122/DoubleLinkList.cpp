#include <iostream>
#include "DoubleLinkList.h"

DLlist::DLlist()
{
    DLlist list;
    DLNode* newNode = new DLNode;
    newNode->data = NULL;
    newNode->next = newNode->prev = NULL;
    list.head = list.tail = NULL;
    list.len = 0;
}

void DLlist::InsertDLlistTail(DLlist& list,ElementType element)
{
    DLNode* newNode = new DLNode;
    newNode->data = element;
    list.tail->next = newNode;
    newNode->prev = list.tail;
    list.tail = newNode;
    list.len++;
}

void DLlist::InsertDLlistHead(DLlist& list, ElementType element)
{
    DLNode* newNode = new DLNode;
    if(list.len == 0)
        list.InsertDLlistTail(list,element);
    else
        newNode->next = list.head->next;
        newNode->prev = list.head;

        list.head->next->prev = newNode;
        list.head->next = newNode;
        list.len++;
}

void DLlist::RemoveByIndex(DLlist &list, int index, void (*freeptr)(ElementType))
{
    if(index < 0 || index >= list.len)
    {
        std::cout<<"删除位置非法: "<<index<<list.len<<std::endl;
        return;
    }
    if(index == list.len - 1)
    {
        DLNode* prev = list.tail->prev;
        prev->next = NULL;
        if(freeptr != NULL)
            freeptr(list.tail->data);
        delete list.tail;
        list.tail = prev;
    }
    else
    {
        DLNode* travelPoint = list.head;
        for(int i = 0; i < index; i++)
        {
            travelPoint = travelPoint->next;
        }

        DLNode* freeNode = travelPoint->next;
        travelPoint->next = freeNode->next;
        freeNode->next->prev = travelPoint;
        if(freeptr != NULL)
            freeptr(freeNode->data);
        delete freeNode;
    }
    list.len--;
}

DLlist DLlist::FindByElement(DLlist &list, ElementType element, bool (*OperPtr)(ElementType, ElementType))
{
    DLlist result;

    DLNode* travelPoint = list.head;
    while(travelPoint->next != NULL)
    {
        if(OperPtr(travelPoint->next->data,element) == true)//如果这个节点数据域是我们要找的元素则↓
        {
            InsertDLlistTail(result,travelPoint->next->data);//插入新的链表里
        }
        travelPoint = travelPoint->next;//不是，遍历指针继续遍历寻找
    }
    return result;//结束返回给函数
}


void DLlist::TravelDLlist(DLlist& list, void (*funcptr)(ElementType))
{
    if(funcptr == NULL)
    return;
    
    DLNode* travelPoint = list.head->next;
    while(travelPoint != NULL)
    {
        funcptr(travelPoint->data);
        travelPoint = travelPoint->next;
    }
}

ElementType *DLlist::FindByIndex(DLlist& list, int index)
{
    if(index < 0 || index >= list.len)
    {
        printf("非法的查找位置\n");
        return NULL;//返回值为值
    }
    DLNode* travelPoint = list.head->next;
    for(int i = 0;i < index;i++)
    {
        travelPoint = travelPoint->next;
    }
    return &travelPoint->data;
}

void DLlist::RemoveByElement(DLlist& list, ElementType element, bool (*Operptr)(ElementType, ElementType), void (*freeptr)(ElementType))
{
    DLNode* travelPoint = list.head;
    int index = 0;
    while(travelPoint->next != NULL)
    {
        if(Operptr(travelPoint->next->data,element) == true)
        {
            RemoveByIndex(list,index,freeptr);
        }
        else
        {
           index++;
           travelPoint = travelPoint->next;
        }
    }
}

void swap(ElementType* value1,ElementType* value2)
{
    ElementType temp = *value1;
    *value1 = *value2;
    *value2 = temp;
}

void DLlist::DLLlistSort(DLlist *list, bool (*funptr)(ElementType, ElementType))
{
    FastSort(list->head->next,NULL,funptr);
}

void DLlist::FastSort(DLNode *start, DLNode *end, bool (*funcptr)(ElementType, ElementType))
{
    if(start == end)
    {
        return;
    }
    DLNode* p1 = start;
    DLNode* p2 = start->next;
    
    while(p2 != end)//经过一轮排序p1为比基准值小的末尾，p2指向基准值大的末尾
    {
        if(funcptr(p2->data,start->data) == true)
        {
            p1 = p1->next;
            swap(&p1->data,&p2->data);
        }
        p2 = p2->next;
    }
    swap(&start->data,&p1->data);

    FastSort(start,p1,funcptr);
    FastSort(p1->next,p2,funcptr);
}

DLlist::~DLlist()
{
    DLlist list;
    while(list.head != NULL)
    {
        DLNode* next = list.head->next;
        if(list.head->next != NULL)
            delete list.head;
        list.head = next;
    }
    list.len = 0;
}

