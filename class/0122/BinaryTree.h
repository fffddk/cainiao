#ifndef _BINARYTREE_H_
#define _BINARYTREE_H_

#define ElementType int

class BTNode
{
    friend class BTSree;
public: 
    BTNode(){};
    static BTNode* CreateBTNode(ElementType element);
    static void TravelNodePrev(BTNode* node);
    static void InsertNode(BTNode* node,ElementType element,int(*funcPtr)(ElementType,ElementType));
    static void TravelNodeMid(BTNode* node,void(*funcPtr)(ElementType));
    static void TravelNodePost(BTNode* node);
    static BTNode* FindNode(BTNode* node,ElementType element);
    ElementType GetNodeData(BTNode* n);
    bool IsLeftChild(BTNode* parent,BTNode* child);
    void RemoveByElement(BSTree * tree, ElementType element);
    static int GetNumber(BTNode* node);

private:
    BTNode* left; //左孩子
    BTNode* right; //有孩子
    BTNode* parent; //双亲节点指针，没有双亲为根节点
    ElementType data; //数据域



};





class BSTree
{
    friend class BSTree;
public:
    BTNode* root;
    BSTree* createTree();
    void TravelPrev(BSTree* tree);
    void InsertElement(BSTree* tree,ElementType element,int(*funcPtr)(ElementType,ElementType));
    void TravelMid(BSTree* tree,void(*funcPtr)(ElementType));
    void TravelPost(BSTree* tree);
    static BTNode* FindByElement(BSTree* tree,ElementType element);
    int GetNodeNumber(BSTree* tree);
};

#endif