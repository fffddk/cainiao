#ifndef _LINKTREE_H_
#define _LINKTREE_H_
#include <iostream>
#include <string>
#include <list>
#define TreeElementType std::string


class TNode
{
    friend class LTree;

public:
    TNode(TreeElementType element);
    static TNode *CreateTreeNode(TreeElementType element);
    void ConnectBranch(TNode *child);
    void DisconnectBranch(TNode *child);
    void TravelTreeNode(int depth, void (*funcPtr)(const TreeElementType&));
    TreeElementType GetTreeNodeData();

private:
    TNode *parent;
    std::list<TNode *> childs;
    TreeElementType data;
};






class LTree
{
    friend class TNode;

public:
    LTree();
    TNode *root;
    static LTree *InitLinkTree();
    void TravelTree(void (*funcPtr)(const TreeElementType&));
    int GetTreeHeight();

private:
    int GetNodeHeight(TNode *node);
    void ClearLinkTree();
};

#endif

