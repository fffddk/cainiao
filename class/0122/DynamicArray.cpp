#include<iostream>
#include"DynamicArray.h"


DynamicArray::DynamicArray()
{
     size=10;
     len=0;
     a=new int[size];
}

DynamicArray::DynamicArray(const DynamicArray &array)
{
    std::cout<<"调用深拷贝函数!"<<std::endl;
    //内存拷贝
    len=array.len;
    size=array.size;

    a=new int[size];
    for(int i=0;i<len;i++)
    {
        a[i]=array.a[i];
    }
}

DynamicArray::~DynamicArray()
{
    delete []a;
    size=0;
    len=0;
    std::cout<<"调用析构函数!"<<std::endl;
}

void DynamicArrayTool::InsertTail(DynamicArray& array,int element)
{
    if(array.len==array.size)
    {
        array.size*=2;
        int *newPtr=new int[array.size];
        for(int i=0;i<array.len;i++)
            newPtr[i]=array.a[i];
        delete []array.a;
        array.a=newPtr;
    }
    array.a[array.len++]=element;
}

bool DynamicArrayTool::RemoveByIndex(DynamicArray& array,int index, void (*ClearPtr)(int))
{
    if(index < 0 || index >= array.len)
    std::cout<<"删除非法位置!\n";
    return false;

    if(ClearPtr != NULL)
        ClearPtr(array.a[index]);
    for(int i = index; i < array.len-1;i++)
    {
        array.a[i] = array.a[i+1];
    }
    array.len--;
    return true;
}

bool DynamicArrayTool::RemoveFirstElement(DynamicArray &array, int element, bool (*funcptr)(int, int), void (*ClearPtr)(int))
{
     for(int i = 0; i < array.len; i++)
    {
        if(funcptr(element,array.a[i] == true))
        {
            if(RemoveByIndex(array,i,ClearPtr) == false)
            {
                return false;
            }
            i--;
        }
    }
    return true;
}

bool DynamicArrayTool::RemoveByElement(DynamicArray &array, int element, bool (*funcPtr)(int, int), void (*clearPtr)(int))
{
     for(int i = 0; i < array.len; i++)
    {
        if(funcPtr(element,array.a[i]) == true)
        {
            if(RemoveByIndex(array,i,clearPtr) == false)
                return false;
        }
    }
    return true;
}

DynamicArray DynamicArrayTool::FindElement(DynamicArray &array, int element, bool (*funcPtr)(int, int))
{
    DynamicArray result;
    for(int i = 0; i < array.len; i++)
    {
        if(funcPtr(element,array.a[i]) == true)
        {
            InsertTail(result,array.a[i]);
        }
    }
    return result;
}

void DynamicArrayTool::FastSort(int *s, int start, int end, bool (*funcptr)(int, int))
{
     if(start >= end)
        return;
    int left = start;
    int right = end;
    int temp = s[left];
    while(left < right)
    {
        while(left < right && funcptr(s[right],temp) == true)
        {
            right--;
        }
        if(left < right)
        {
            s[left] = s[right];
            left++;
        }
        while(left < right && funcptr(s[left],temp) == false)
        {
            left++;
        }
        if(left < right)
        {
            s[right] = s[left];
            right--;
        }
        s[left] = temp;

        FastSort(s,start,left-1,funcptr);
        FastSort(s,right+1,end,funcptr);
    }
}

void DynamicArrayTool::show(DynamicArray& array)
{
    std::cout<<"size:"<<array.size<<" len:"<<array.len<<std::endl;
    for(int i=0;i<array.len;i++)
    {
        std::cout<<array.a[i]<<" ";
    }
    std::cout<<std::endl;
}


