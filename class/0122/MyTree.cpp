#include "MyTree.h"
#include <cstdlib>
#include <iostream>

// TNode成员函数实现
TNode::TNode(TreeElementType element) : data(element), parent(nullptr) {}

TNode *TNode::CreateTreeNode(TreeElementType element)
{
    TNode *newNode = new TNode(element);
    return newNode;
}

void TNode::ConnectBranch(TNode *child)
{
    if (child != nullptr)
    {
        child->parent = this;
        childs.push_back(child);
    }
}

void TNode::DisconnectBranch(TNode *child)
{
    if (child != nullptr)
    {
        if (child->parent == this)
        {
            child->parent = nullptr;
            childs.remove(child);
        }
        else
        {
            std::cout << "Error!" << std::endl;
        }
    }
}

void TNode::TravelTreeNode(int depth, void (*funcPtr)(const TreeElementType&))
{
    if (parent != nullptr)
    {
        for (int i = 0; i < depth; i++)
        {
            std::cout << "    ";
        }
        funcPtr(data);
    }

    for (auto child : childs)
    {
        child->TravelTreeNode(depth + 1, funcPtr);
    }
}

TreeElementType TNode::GetTreeNodeData()
{
    return data;
}

// LTree成员函数实现
LTree::LTree() : root(nullptr) {}

LTree *LTree::InitLinkTree()
{
    LTree *tree = new LTree;
    tree->root = TNode::CreateTreeNode("");
    return tree;
}

void LTree::TravelTree(void (*funcPtr)(const TreeElementType&))
{
    root->TravelTreeNode(-1, funcPtr);
}

int LTree::GetNodeHeight(TNode *node)
{
    if (node == nullptr)
        return 0;
    int height = 0;
    for (auto child : node->childs)
    {
        int childHeight = GetNodeHeight(child);
        height = height > childHeight ? height : childHeight;
    }
    return height + 1;
}

int LTree::GetTreeHeight()
{
    return GetNodeHeight(root);
}

void LTree::ClearLinkTree()
{
    if (root != nullptr)
    {
        delete root;
        root = nullptr;
    }
}