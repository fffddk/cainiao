#ifndef __DYNAMICARRAY_H_
#define __DYNAMICARRAY_H_

class DynamicArray
{
    friend class DynamicArrayTool;
public:
    DynamicArray();
    DynamicArray(const DynamicArray& array);
    ~DynamicArray(); 
private:
    int*a;
    int len;
    int size;
};






class DynamicArrayTool
{
public:
    static void InsertTail(DynamicArray& array,int element);
    static bool RemoveByIndex(DynamicArray& array,int index, void(*ClearPtr)(int));
    static bool RemoveFirstElement(DynamicArray& array,int element,bool(*funcptr)(int,int),void(*ClearPtr)(int));
    static bool RemoveByElement(DynamicArray& array,int element,bool(*funcPtr)(int,int),void(*clearPtr)(int));
    static DynamicArray FindElement(DynamicArray& array, int element, bool (*funcPtr)(int, int));
    static void FastSort(int *s,int start,int end,bool(*funcptr)(int,int));
    static void show(DynamicArray& array);


    
};













#endif