#include <iostream>
#include "BinaryTree.h"

BTNode *BTNode::CreateBTNode(ElementType element)
{
    BTNode* newNode = new BTNode;

    newNode->data = element;
    newNode->left = newNode->right = newNode->parent = NULL;
    return newNode;
}

void BTNode::InsertNode(BTNode *node, ElementType element, int (*funcPtr)(ElementType, ElementType))
{
    if(node == NULL)
        return;
    
    if(funcPtr(node->data,element) > 0 && node->left == NULL)
    {
        node->left = CreateBTNode(element);
        if(node->left == NULL)
            return;
        
        node->left->parent = node;
        return;
    }
    
    if(funcPtr(node->data,element) < 0 && node->right == NULL)
    {
        node->right = CreateBTNode(element);
        if(node->right = NULL)
            return;
        
        node->right->parent = node;
            return;
    }

    if(funcPtr(node->data,element) == 0)
        return;
    
    if(funcPtr(node->data,element) > 0)
        InsertNode(node->left,element,funcPtr);
    else
        InsertNode(node->right,element,funcPtr);
}

BSTree * BSTree::createTree()
{
    BSTree* tree = new BSTree;

    tree->root = NULL;
    return tree;
}

void BSTree::InsertElement(BSTree *tree, ElementType element, int (*funcPtr)(ElementType, ElementType))
{
    if(tree->root == NULL)
        tree->root = BTNode::CreateBTNode(element);
    else
        BTNode::InsertNode(tree->root,element,funcPtr);
}

void BTNode::TravelNodePrev(BTNode * node)
{
    if(node == NULL)
        return;
    
    std::cout<<node->data<<std::endl;
    TravelNodePrev(node->left);
    TravelNodePrev(node->right);
}

void BSTree::TravelPrev(BSTree *tree)
{
    BTNode::TravelNodePrev(tree->root);
}

void BTNode::TravelNodeMid(BTNode *node, void (*funcPtr)(ElementType))
{
    if(node == NULL)
        return;
    
    TravelNodeMid(node->left,funcPtr);
    funcPtr(node->data);
    TravelNodeMid(node->right,funcPtr);
}

void BSTree::TravelMid(BSTree *tree, void (*funcPtr)(ElementType))
{
    BTNode::TravelNodeMid(tree->root,funcPtr);
}

void BTNode::TravelNodePost(BTNode *node)
{
        if(node == NULL)
        return;

    TravelNodePost(node->left);
    TravelNodePost(node->right);
    std::cout<<node->data<<std::endl;
}

void BSTree::TravelPost(BSTree * tree)
{
    BTNode();
    BTNode::TravelNodePost(tree->root);
}

BTNode * BTNode::FindNode(BTNode * node, ElementType element)
{
    if(node == NULL)
    return NULL;
    
    if(node->data == element)
        return node;

    else if(node->data > element)
    {
        return FindNode(node->left,element);
    }
    else
    {
        return FindNode(node->right,element);
    }
}

BTNode * BSTree::FindByElement(BSTree * tree, ElementType element)
{
    return BTNode::FindNode(tree->root,element);
}


ElementType BTNode::GetNodeData(BTNode * n)
{
    return n->data;
}

bool BTNode::IsLeftChild(BTNode *parent, BTNode *child)
{
    if(parent->left == child)
        return true;
    else
        return false;
}

void BTNode::RemoveByElement(BSTree *tree, ElementType element)
{
    BTNode* node = BSTree::FindByElement(tree,element);//先找到该节点 
    
    if(node == NULL)
        return;
    //叶子（没有孩子）
    if(node->left == NULL && node->right == NULL)
    {
        //如果是根节点
        if(node->parent ==  NULL)
        {
            free(node);
            tree->root = NULL;
            return;
        }

        if(IsLeftChild(node->parent,node) == true)//待删节点是左孩子
            node->parent->left = NULL;//直接删除
        else
            node->parent->right = NULL;//右孩子直接删除

        free(node);
    }

    //左孩子不空，右孩子空（非叶子）
    else if(node->right == NULL)
    {
        node->left->parent = node->parent;
        if(node->parent == NULL)
        {
            tree->root = node->left;
            free(node);
            return;
        }
        if(IsLeftChild(node->parent,node) == true)//待删节点左孩子继承
            node->parent->left = node->left;
        else
            node->parent->right = node->left;//待删节点右孩子继承
        free(node);
    }

    //左孩子空，右孩子不空
    else if(node->left == NULL)
    {
        node->right->parent = node->parent;//右孩子的双亲一定会变为待删节点的双亲
        if(node->parent == NULL)//为根
        {
            tree->root = node->right;
            free(node);
            return;
        }
        
        if(IsLeftChild(node->parent,node) == true)
            node->parent->left = node->right;
        else
            node->parent->right = node->right;
        free(node);
    }

    //左右孩子都不为空
    else
    {
        //找右边子树最小的点放过来
        BTNode* MinNode = node->right;//此处不考虑节点是否为根是因为，下面是用换值删除MinNode
        while(MinNode->left != NULL)//一直找到右边节点中最小的那个节点
        {
            MinNode = MinNode->left;
        }
        
        node->data = MinNode->data;//交换两个节点的值，删除最小的节点

        //数据已经更换，下面是针对待删除元素MinNode的不同情况讨论
        //如果右子树最小节点碰巧就是右孩子
        if(MinNode->parent == node)
            node->right = MinNode->right;

        else
        //最左边的点可能有右孩子
            MinNode->parent->left = MinNode->right;
        free(MinNode);
    }
}

int BTNode::GetNumber(BTNode *node)
{
    if(node == NULL)
        return 0;
    
    return GetNumber(node->left) + GetNumber(node->right) + 1;
}

int BSTree::GetNodeNumber(BSTree *tree)
{
    return BTNode::GetNumber(tree->root);
}