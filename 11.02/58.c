#include<stdio.h>
#define N 6
int main(){
    int j,i,k,t;
    printf("The possible Latin Squares of order %d are:\n",N);
    for(j=0;j<N;j++){
      for(i=0;i<N;i++){
        t=(i+j)%N;
        for(k=0;k<N;k++)
        printf("%d",(k+t)%N+1);
        printf("\n");
      } 
       printf("\n");
    }
}