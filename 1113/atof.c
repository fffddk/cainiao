#include <stdio.h>

//定义最大数组长度
#define StrExtFloat_flen 64
//定义最大字符串长度
#define StrExtFloat_slen 512

void StrExtFloat(float *num,char* Str)
{
	//遍历深度
	int Fflag = 0;
	//数字个数
	int Fnum = 0;
	char num_start = 0,num_point = 0;
	//遍历到字符串尾部
	while ( *Str != '\0' )
	{	
		Fflag++;
		//防止查询超过边界
		if(Fflag>StrExtFloat_slen)
			break;
		//判断是不是数字
		if(*Str >='0' && *Str <= '9')
		{
			//printf("%c",*Str);
			//判断数字存在
			num_start = 1;
			//判断是否存在小数点
			if(num_point >= 1)
			{
				num_point++;
				//当前小数部分的数值
				float fpoint = *Str - '0';
				for(int i = 1;i<num_point;i++)
				{
					fpoint = fpoint/10.;
				}
				//加入小数部分
				num[Fnum+1] = num[Fnum+1] + fpoint;
			}
			else
			{
				//加入整数部分
				num[Fnum+1] = num[Fnum+1]*10+(*Str - '0');
			}
		}
		else if(*Str == '.') //判断为小数点
		{
			if(num_start==1)//发现存在小数点
			{
				num_point=1;
			}
		}
		else //判断为其他字符
		{
			if (num_start == 1)
			{
				Fnum++;//统计个数加一
			}
			//清空字符统计与小数点统计
			num_start = 0;
			num_point = 0;
		}
		//指针移动
		*(Str++);
	}
	//如果不是以字符结尾
	if (num_start == 1)
	{
		Fnum++;//统计个数加一
	}
	//放入提取到的数字个数
	num[0] = Fnum;
}


void main(void){
	char Str[100]="235.654hjfv92.88u98fj3wjf09w43f0f3f963.369";
	float num[StrExtFloat_flen] = {0.};
	
	void StrExtFloat(float *num,char* Str);
	StrExtFloat(num,Str);
	printf("\n Total:%2.0f",num[0]);
	for(int i=1;i<=num[0];i++)
		printf("\n %d:%.4f",i,num[i]);	
}

