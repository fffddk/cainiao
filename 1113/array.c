#include<stdio.h>
void PrintArray(int (*a)[3])
{
    for(int i=0;i<2;i++)
    {
        for(int j=0;j<3;j++){

        printf("%x ",&a[i][j]);}
        printf("\n");
    }
}


int main()
{
    //定义一个两行三列的二维数组
    //初始化
    int a[2][3]={{1,2,3},{4,5,6}};
    PrintArray(a);
    int (*p)[3]=a;
    printf("%x\n",**(p+1));
  //二维数组在内存中的排布:连续排布
    //PrintArray(a);

    //a的理解
    //printf("%d\n",sizeof(a));
    //a:一维数组指针
    //&a:二维数组指针
    //&a[0]:一维数组指针
    //&a[0][0]:数组元素指针
    //printf("%x %x %x %x\n",a,&a,&a[0],&a[0][0]);
    //printf("%x %x %x %x\n",a+1,&a+1,&a[0]+1,&a[0][0]+1);
    //printf("%d\n",**(a+1));
    //printf("%d\n",*(a[0]+1));
    //int* p[2];
    //p[0]=a;//p[0]指向a的第一行的一维数组
    //p[1]=a+1;//p[1]指向a的第二行的一维数组

    //a==&a[0]

    printf("%d\n",**(p+1));
    return 0;

}