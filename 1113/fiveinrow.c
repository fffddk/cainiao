#include<stdio.h>
#include<stdbool.h>
#define size 15

void InitBoard(char (*board)[size])
{
    for(int i=0;i<size;i++)
    {
        for(int j=0;j<size;j++)
        {
            board[i][j] = '-';
        }
    }
}

void PrintBoard(char (*board)[size])
{
    printf("    ");
    for(int i = 0;i<size;i++)
    {
        printf("%4d",i+1);
    }
    printf("\n");
    for(int i = 0;i<size;i++)
    {
        printf("%4d",i+1);
        for(int j = 0;j<size;j++)
        {
            printf("%4c",board[i][j]);
        }
        printf("\n");
    }
}
bool MakeMove(char (*board)[size],int row,int column,char player)
{
    if(row<0||row>=size||column<0||column>=size)
    {
        printf("非法位置，请重新选择位置");
        return false;
    }
    if(board[row][column]!='-')
    {
        printf("该位置已经有棋子了，请重新选择位置\n");
        return false;
    }
    board[row][column] = player;
    return true;
}
bool CheckIsDraw(char (*board)[size])
{
    for(int i = 0;i<size;i++)
    {
        for(int j = 0;j<size;j++)
        {
            if(board[i][j] == '-')
            return false;
        }
    }
    return true;
}
bool CheckIswin(char (*board)[size],int row,int column,char player)
{
    int direction[4][2] = 
    {
        {1,0},{0,1},{1,1},{-1,1}
    };
    for(int i = 0;i < 4;i++)
    {
        int count = 1;
        int dx = row + direction[i][0];
        int dy = column + direction[i][1];
        while (dx>=0&&dx<=size&&dy>=0&&dy<size&&board[dx][dy] == player)
        {
            count++;
            dx = dx + direction[i][0];
            dy = dy + direction[i][1];
            if(count>=5)
            {
                return true;
            }
        }
        dx = row - direction[i][0];
        dy = column - direction[i][1];
        while (dx>=0&&dx<=size&&dy>=0&&dy<=size&&board[dx][dy] == player)
        {
            count++;
            dx = dx - direction[i][0];
            dy = dy - direction[i][1];
            if(count>=5)
            {
                return true;
            }
        }
        if(count >= 5)
        return true;
    }
    return false;
}

int main()
{
    char Board[size][size] = {0};
    InitBoard(Board);
    PrintBoard(Board);
    //PrintBoard(board);
    char player = 'x';
    while (1)
    {
        PrintBoard(Board);
        int row = 0,column = 0;
        printf("请选手%c下棋:",player);
        scanf("%d %d",&row,&column);
        if(MakeMove(Board,row,column,player)==false)
        {
            continue;
        }
        if(CheckIswin(Board,row,column,player) == true)
        {
            PrintBoard(Board);
            printf("选手%c获得了胜利!\n",player);
            break;
        }
        if(CheckIsDraw(Board) == true)
        {
            printf("平局！游戏结束!\n");
            break;
        }
        player = (player == 'x')?'o':'x';
    }
    return 0;
}