//水仙花数
#include<stdio.h>
#define f(x) x*x*x    //求三次方时可以节省打代码的时间
int main(void)
{
	int i;
	for(i=100;i<=999;i++)
	{
		int a,b,c;            //a,b,c求各位上的数
		a=i/100;      
		b=i/10%10;
		c=i%10;
		if(i==f(a)+f(b)+f(c))printf("%-5d",i);    //用宏定义求三次方，判断是否满足水仙花数的条件，若满足，则输出。
    }
	return 0;
}

