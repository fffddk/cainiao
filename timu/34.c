#include <stdio.h>
int main(int argc,const char *argv[])
{
    int a[3][4] = {{123,94,-10,218},
                   {3,9,10,-83},
                   {45,16,44,-99}};
    int i,j=0;
    int max_row[3]={0};
    int max_col[4]={0};
    for(i=0;i<3;i++)
    {
        for(j=0;j<4;j++)
        {
            if(a[i][j]>max_row[i])
            {
                max_row[i] = a[i][j];
            }
        }
    }
    for(j=0;j<4;j++)
    {
        for(i=0;i<3;i++)
        {
            if(a[i][j]>max_col[j])
            {
                max_col[j]=a[i][j];
            }
        }
    }
    for(i=0;i<3;i++)
    {
        for(j=0;j<4;j++)
        {
            if(max_col[j]==max_row[i])
            {
                printf("%d %d %d\n",max_row[i],i,j);
            }
        }
    }
    return 0;
}
