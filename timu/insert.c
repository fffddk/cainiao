//插入排序
void InsertSort(int *a,int len)
{
    for(int i=1;i<len;i++)
    {
        int temp=a[i];
        int j=i;
        for(;j>0;j--)
        {
            if(a[j-1]>temp)
            {
                a[j]=a[j-1];
            }
            else
            {
                break;
            }
        }
        a[j]=temp;
    }
}