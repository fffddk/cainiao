// 定义一个函数，参数为一个int类型的值，返回值为一个int类型的值
int count_ones(int n) {
    // 定义一个变量count，用来记录1的个数，初始值为0
    int count = 0;
    // 使用一个循环，每次将n和1进行按位与运算，如果结果为1，说明n的最低位是1，count加1
    // 然后将n右移一位，继续判断下一位，直到n为0为止
    while (n != 0) {
        if (n & 1 == 1) {
            count++;
        }
        n = n >> 1;
    }
    // 返回count的值
    return count;
}
