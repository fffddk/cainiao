//选择排序
void ChooseSort(int *a,int len)
{
  int left=0;
  int right=len-1;
  while(left<right)
  {
    int min=left;
    int max=right;
    for(int i=left;i<=right;i++)
    {
        if(a[i]<a[min])
        {
            min=i;
        }
        if(a[i]>a[max])
        {
            max=i;
        }
    }
    int temp=a[left];
    a[left]=a[min];
    a[min]=temp;

    if(max==left)
    {
        max=min;
    }

    temp=a[right];
    a[right]=a[max];
    a[max]=temp;

    left++;
    right--;

  }
}