#include <stdio.h>
#include <stdbool.h>
#include <limits.h>

// 定义一个函数，判断一个整数是否是回文数
bool isPalindrome(int x) {
    // 如果x是负数，或者x的最后一位是0且x不等于0，那么x不是回文数
    if (x < 0 || (x % 10 == 0 && x != 0)) {
        return false;
    }
    // 定义一个变量rev，用来存储x的反转后的一半
    int rev = 0;
    // 循环，直到x小于等于rev，表示已经处理了x的一半以上的位数
    while (x > rev) {
        // 将x的最后一位加到rev的末尾，同时将x除以10，去掉最后一位
        rev = rev * 10 + x % 10;
        x /= 10;
    }
    // 如果x等于rev，或者x等于rev除以10（去掉奇数位的中间一位），那么x是回文数
    return x == rev || x == rev / 10;
}

// 定义一个主函数，测试上面的函数
int main() {
    // 定义一个测试用例数组，包含5个整数
    int test_cases[] = {121, -121, 10, 0, INT_MAX};
    // 循环，对每个测试用例调用isPalindrome函数，并打印结果
    for (int i = 0; i < 5; i++) {
        int x = test_cases[i];
        printf("%d is %s palindrome\n", x, isPalindrome(x) ? "a" : "not a");
    }
    // 返回0，表示程序正常结束
    return 0;
}




