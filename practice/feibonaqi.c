


//递归求斐波那契数列
int fib(int n){
    if(n <= 1)
        return n;
    return fib(n-1) + fib(n-2);
}