#include<stdio.h>
#include<math.h>
int* twoSum(int* nums, int numsSize, int target, int* returnSize) {
    for (int i = 0; i < numsSize; ++i)
     {
        for (int j = i + 1; j < numsSize; ++j) 
        {
            if (nums[i] + nums[j] == target) 
            {
                int* ret = malloc(sizeof(int) * 2);//如果找到满足条件的两个数,创建一个包含两个整数的动态数组
                ret[0] = i, ret[1] = j;
                *returnSize = 2;//通过指针将结果数组的大小设置为2,返回动态数组
                return ret;
            }
        }
    }
    *returnSize = 0;
    return NULL;
}
