//罗马数字转整数
#include <stdio.h>

int romanToInt(char* s) {
    int num = 0; // 用于存储最终的整数结果
    int i = 0;

    while (s[i] != '\0') { // 遍历输入的罗马数字字符串
        // 判断当前字符和下一个字符的大小关系
        if (s[i] == 'I' && (s[i+1] == 'V' || s[i+1] == 'X')) {
            // 如果当前字符是'I'且下一个字符是'V'或'X'，则表示是小数在大数的左边，需要减去当前数值
            num -= 1;
        } else if (s[i] == 'X' && (s[i+1] == 'L' || s[i+1] == 'C')) {
            // 如果当前字符是'X'且下一个字符是'L'或'C'，则表示是小数在大数的左边，需要减去当前数值
            num -= 10;
        } else if (s[i] == 'C' && (s[i+1] == 'D' || s[i+1] == 'M')) {
            // 如果当前字符是'C'且下一个字符是'D'或'M'，则表示是小数在大数的左边，需要减去当前数值
            num -= 100;
        } else {
            // 其他情况，表示当前字符是一个单独的数值，直接加上其对应的整数值
            if (s[i] == 'I') {
                num += 1;
            } else if (s[i] == 'V') {
                num += 5;
            } else if (s[i] == 'X') {
                num += 10;
            } else if (s[i] == 'L') {
                num += 50;
            } else if (s[i] == 'C') {
                num += 100;
            } else if (s[i] == 'D') {
                num += 500;
            } else if (s[i] == 'M') {
                num += 1000;
            }
        }
        i++; // 继续处理下一个字符
    }

    return num; // 返回最终的整数结果
}

int main() {
    char romanNum[100];
    printf("请输入罗马数字: ");
    scanf("%s", romanNum);
    int result = romanToInt(romanNum);
    printf("转换后的整数为: %d\n", result);
    return 0;
}