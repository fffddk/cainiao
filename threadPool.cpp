#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <functional>
#include <queue>
#include <atomic>
#include <vector>
#include <chrono>
#include <memory>

class ThreadPool {
public:
    using Task = std::function<void()>;

    explicit ThreadPool(int maxThreadNumber, int minThreadNumber, int maxQueueSize, int second)
        : maxThreadNumber(maxThreadNumber),
          minThreadNumber(minThreadNumber),
          maxQueueSize(maxQueueSize),
          sleepTime(second),
          busyThreadNum(0),
          shutDown(false) {
        Initialize();
    }

    ~ThreadPool() {
        ShutDown();
    }

    void AddTask(const Task& task) {
        std::unique_lock<std::mutex> lock(poolMutex);
        queueNotEmpty.wait(lock, [this]() { return !tasks.empty() || shutDown; });

        if (shutDown) {
            return;
        }

        tasks.push(task);
        queueNotFull.notify_one();
    }

private:
    std::vector<std::thread> threads;
    std::queue<Task> tasks;
    std::mutex poolMutex;
    std::condition_variable queueNotEmpty;
    std::condition_variable queueNotFull;

    int maxThreadNumber;
    int minThreadNumber;
    int maxQueueSize;
    int sleepTime;
    std::atomic<int> busyThreadNum;
    std::atomic<bool> shutDown;

    void Initialize() {
        for (int i = 0; i < maxThreadNumber; ++i) {
            threads.emplace_back(&ThreadPool::ThreadHandler, this);
        }

        std::thread managerThread(&ThreadPool::ThreadManager, this);
        threads.emplace_back(std::move(managerThread));
    }

    void ThreadHandler() {
        while (true) {
            std::unique_lock<std::mutex> lock(poolMutex);
            queueNotEmpty.wait(lock, [this]() { return !tasks.empty() || shutDown; });

            if (shutDown) {
                return;
            }

            Task task = std::move(tasks.front());
            tasks.pop();
            queueNotFull.notify_one();
            lock.unlock();

            // 退出
            task();

            lock.lock();
            --busyThreadNum;
            lock.unlock();
        }
    }

    void ThreadManager() {
        while (!shutDown) {
            std::this_thread::sleep_for(std::chrono::seconds(sleepTime));

            std::unique_lock<std::mutex> lock(poolMutex);

            // 如果任务数量大于线程数量就增加线程
            if (tasks.size() > threads.size() && threads.size() < maxThreadNumber) {
                int maxAddNum = maxThreadNumber - threads.size();
                int shouldAddNum = tasks.size() / 2;
                int addNumber = std::min(maxAddNum, shouldAddNum);
                for (int i = 0; i < addNumber; ++i) {
                    threads.emplace_back(&ThreadPool::ThreadHandler, this);
                }
            }

            // 如果忙碌线程数量少于总线程的一半就删除线程
            if (busyThreadNum * 2 < threads.size() && threads.size() > minThreadNumber) {
                int maxMinusNum = threads.size() - minThreadNumber;
                int shouldMinusNum = (threads.size() - busyThreadNum) / 2;
                int minusNum = std::min(maxMinusNum, shouldMinusNum);
                for (int i = 0; i < minusNum; ++i) {
                    queueNotEmpty.notify_one();
                }
            }
        }
    }

    void ShutDown() {
        shutDown = true;
        queueNotEmpty.notify_all();

        for (std::thread& thread : threads) {
            if (thread.joinable()) {
                thread.join();
            }
        }
    }
};



int main() {
    ThreadPool pool(10, 5, 100, 1);

    for (int i = 0; i < 20; ++i) {
        pool.AddTask([i]() {
            std::cout << "Task " << i << " executed by thread " << std::this_thread::get_id() << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        });
    }

   
    std::this_thread::sleep_for(std::chrono::seconds(30));

    return 0;
}


