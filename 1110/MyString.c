#include<stdio.h>
int MyStrlen(const char *s)
{
   int count=0;
   while(*s!=0)
   {
    s++;
    count++;
   }
   return count;
}

void MyStrcpy(char *dest,const char * src)
{
    while( *src!='\0')
    {
        *dest++=*src++;

    }
    *dest='\0';
}



void MyStrcat(char *dest,const char*src)
{
    while(*dest!='\0')
    {
        dest++;
    }
    MyStrcpy(dest,src);
}


//brute-force:暴力匹配算法
//KMP算法
char* MyStrstr(const char *s,const char* d)
{
    for(int i=0;s[i]!='\0';i++)
    {
        int flag=0;
        for(int j=0;d[j]!='\0';j++)
        {
            //没匹配上
            if(s[i+j]!=d[j])
        {
           flag=1;
           break;
        }
        } 
        if(flag==0)
        {
            return s+i;
        }
    }
    return NULL;
}

char* MyStrtok(char *dest,const char*demil)
{
    static char * next=NULL;
    if(dest!=NULL)
    {
    char* target=MyStrstr(dest,demil);
    if(target==NULL)
    return dest;
    *target='\0';
    next=target+MyStrlen(demil);
    return dest;
    }
   else
   {
     if(next==NULL)
     {
        return NULL;
     }
     else
     {
        //没找到匹配的切割字符
      char* target=MyStrstr(next,demil);
     if(target==NULL)
     {
     char *returnValue=next;
     next=NULL;
     return returnValue;
     }
     *target='\0';
    char *returnValue=next;
     next=target+MyStrlen(demil);
     if(*next=='\0')
     {
        next=NULL;
     }
     return returnValue;
   }
}
}


int MyStrcmp(const char* s1,const char*s2)
{
    while (*s1!='\0'&&*s2!='\0')
    {
        if(*s1!=*s2)
        {
            return *s1-*s2;
        }
        s1++;
        s2++;
    }
    return *s1-*s2;
    
}


int main()
{ 
    char s[]="zhangsan,lisi,wangwu";
    char *p=MyStrtok(s,",");
    while(p!=NULL)
    {
        printf("%s\n",p);
        p=MyStrtok(NULL,",");
    }
    //字符串切割常用函数模板
    return 0;
}