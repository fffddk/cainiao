#include<stdio.h>
//一只猴子采了n个桃子，每天都吃剩下的桃子的一半多一个，到第10天剩一个，原来有几个桃子
int EatPeach(int day)
{
    if(day==10)//中止条件
    return 1;
    return (EatPeach(day+1)+1)*2;
}


//用递归实现数的阶乘
int JCen(int n)
{
    if(n==1)
    return 1;
    else
    return n*JCen(n-1);
}


//计算字符串长度 strlen  递归
int MyStrlen(char *s)
{
  if(*s=='\0')
  return 0;
  else
  return 1+MyStrlen(s+1);
}


int main()
{
    printf("%d\n",EatPeach(10));
    return 0;
}