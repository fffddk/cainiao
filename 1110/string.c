#include<stdio.h>
#include<string.h>
//strlen 计算字符串的长度，不包括\0
//strcpy 字符串拷贝：本身不会进行越界处理
//strncpy
//strcat 字符串拼接：不会进行越界处理
//strncat
//strstr 字符串匹配函数，成功返回第一个字符串的匹配位置，失败返回空指针
//strtok 字符串切割函数:切到无字符串时，返回NULL  会修改源字符串
//strcmp 字符串比较函数 当返回值为0，两个字符串相等  （用对应位置的字符相减）
//strncmp

//字符串转整型
int atoi(const char *s)
{
  int result=0;
  int sign=1;
  if(s[0]=='-')
  {
    sign=-1;
  }
  while (*s!='/0')
  { if(*s<='9'&&*s>='0')
    result=result*10+*s-'0';
    s++;
  }
  
  return result*sign;
}

int main()
{
//字符数组的定义
//字符数组的初始化
//字符串赋除值
    char s[]="123";
    printf("%d\n",atoi(s));

    
    
    return 0;
}