//strlen函数返回的是在字符串中’\0’前面出现的字符的个数
#include<stdio.h>
#include<string.h>
size_t MyStrlen(const char* str1)//模拟实现strlen函数
{
	size_t len = 0;
	while (*str1 != 0)
	{
		++len;
		++str1;
	}
	return len;
}

int main()
{
	char str1[] = "abcdef";
	printf("%ld\n", strlen(str1));
	return 0;
}
