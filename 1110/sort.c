#include<stdio.h>
//排序算法
//冒泡排序
void BubbleSort(int *a,int len)
{
    int flag=0;
    for(int i=0;i<len-1;i++)
    {
      for(int j=0;j<len-1-i;j++)
      {
        if(a[j]>a[j+1])
        {
            flag=1;
            int temp=a[j];
            a[j]=a[j+1];
            a[j+1]=temp;
        }
      }
      if(flag==0)//数列已经有序
      {
        break;
      }
    }
}

//选择排序:每次选择剩下数列中最小的一个数放到前面
void ChooseSort(int *a,int len)
{
  for(int i=0;i<len;i++)
  {
    int min=i;
    for(int j=i+1;j<len;j++)
    {
        if(a[j]<a[min])
        {
            min=j;
        }
    }
     int temp=a[i];
     a[i]=a[min];
     a[min]=temp;

  }
}

//选择排序的优化
void ChooseSort2(int *a,int len)
{
  int left=0;
  int right=len-1;
  while(left<right)
  {
    int min=left;
    int max=right;
    for(int i=left;i<=right;i++)
    {
        if(a[i]<a[min])
        {
            min=i;
        }
        if(a[i]>a[max])
        {
            max=i;
        }
    }
    int temp=a[left];
    a[left]=a[min];
    a[min]=temp;

    if(max==left)
    {
        max=min;
    }

    temp=a[right];
    a[right]=a[max];
    a[max]=temp;

    left++;
    right--;

  }
}



void PrintArray(int *a,int len)
{
    for(int i=0;i<len;i++)
    {
        printf("%d ",a[i]);
    }
    printf("\n");
}

//插入排序
void InsertSort(int *a,int len)
{
    for(int i=1;i<len;i++)
    {
        int temp=a[i];
        int j=i;
        for(;j>0;j--)
        {
            if(a[j-1]>temp)
            {
                a[j]=a[j-1];
            }
            else
            {
                break;
            }
        }
        a[j]=temp;
    }
}




//快速排序
void FastSort(int *a,int start,int end)
{
    //终止条件
    if(start>=end)
    {
        return;
    }
    int temp=a[start];
    int left=start;
    int right=end;
    while(left<right)
    {
       while(left<right&&a[right]>temp)
       {
        right--;
       }
       if(left<right)
       {
        a[left]=a[right];
        left++;
       }
       while(left<right&&a[left]<temp)
       {
        left++;
       }
       if(left<right)
       {
        a[right]=a[left];
        right--;
       }
    }
    a[left]=temp;
    //递归
    FastSort(a,start,left-1);
    FastSort(a,right+1,end);
}


//顺序查找
int FindElementByIndex(int *a,int len,int target)
{
    for(int i=0;i<len;i++)
    {
        if(a[i]==target)
        return i;
    }
    return -1;
}

//二分查找
int BinaryFind(int *a,int len,int target)
{
    int left=0;
    int right=len-1;
    while(left<=right)
    {
        int mid=left+(right-left)/2;//注意数据溢出
        if(a[mid]==target)
        return mid;
        if(a[mid]>target)
        right=mid-1;
        if(a[mid]<target)
        left=mid+1;
    }
    return -1;
}




int main()
{
 int a[]={9,2,16,98,54,1,45};
 FastSort(a,0,6);
 PrintArray(a,sizeof(a)/4);
 return 0;

}

//
